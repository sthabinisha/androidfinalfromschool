package com.und.ex3mapsandroid;

import android.support.v4.app.Fragment;

import com.und.ex3mapsandroid.R;
import com.und.ex3mapsandroid.base.BaseFragment;
import com.und.ex3mapsandroid.model.Restaurant;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;



public class AboutUs extends BaseFragment {
    public static AboutUs newInstance() {
        AboutUs aboutUs = new AboutUs();

        return aboutUs;
    }

    @Override
    public int layout() {
        return R.layout.fragment_about_us;
    }

    @Override
    public void refreshData(List<Restaurant> restaurantList) {

    }

    @Override
    public void onLocationUpdate(LatLng latLng) {

    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}
