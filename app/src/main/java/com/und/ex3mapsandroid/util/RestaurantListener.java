package com.und.ex3mapsandroid.util;

import android.support.v4.app.Fragment;

import com.und.ex3mapsandroid.model.Restaurant;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;



public interface RestaurantListener {

    void onRestaurantListUpdate(List<Restaurant> restaurantList);

    void onLocationUpdate(LatLng latLng);

    Fragment getFragment();
}
