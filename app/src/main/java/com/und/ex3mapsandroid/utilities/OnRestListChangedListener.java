package com.und.ex3mapsandroid.utilities;

import com.und.ex3mapsandroid.model.Restaurant;

import java.util.List;



public interface OnRestListChangedListener {
    void onRestListOrderChanged(List<Restaurant> restaurants);
}
