package com.und.ex3mapsandroid.utilities;

import android.support.v7.widget.RecyclerView;



public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
