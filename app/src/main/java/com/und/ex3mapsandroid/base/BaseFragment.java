package com.und.ex3mapsandroid.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.und.ex3mapsandroid.model.Restaurant;
import com.und.ex3mapsandroid.util.RestaurantListener;

import java.util.List;



public abstract class BaseFragment extends Fragment implements RestaurantListener {

    public abstract int layout();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(layout(), container, false);
    }

    @Override
    public void onRestaurantListUpdate(List<Restaurant> restaurantList) {
        refreshData(restaurantList);
    }

    public abstract void refreshData(List<Restaurant> restaurantList);
}
